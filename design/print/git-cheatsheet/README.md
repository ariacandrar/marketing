# GitLab Git cheatsheet

>
The GitLab Git cheatsheet was first created by [Tomasz](https://gitlab.com/tmaczukin)
for the Rails Girls Summer of Code.

In the [_artwork](./_artwork) directory you can find the source of the SVG files
that are used to create the final PDF which resides in [print-pdf](./print-pdf).

Below you can find some useful information to convert between the formats.

## Convert SVG to PDF

_Source: http://graphicdesign.stackexchange.com/a/56792._

---

1. Install [inkscape].
1. Convert to PDF. There are two ways to do that:

   - Open the SVG files with inkscape and save them as PDF,
   - Or use the command line:

     ```
     inkscape --file=git-cheatsheet-page01.svg --export-area-drawing --without-gui --export-pdf=git-cheatsheet-page01.pdf
     inkscape --file=git-cheatsheet-page02.svg --export-area-drawing --without-gui --export-pdf=git-cheatsheet-page02.pdf
     ```

## Concatenate the two PDFs into one

_Source: http://superuser.com/a/54043._

---

To concatenate the two PDFs into one:

1. Install [ghostscript].
1. From your terminal:

    ```
    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=git-cheatsheet.pdf git-cheatsheet-page01.pdf git-cheatsheet-page02.pdf
    ```

## Updating the cheatsheet

To update the cheatsheet:

1. Edit the SVGs inside the `_artwork` directory.
1. Save them.
1. Convert to PDF and concatenate them according to the sections above. Make
   sure you're in the `_artwork` directory. TL;DR:

    ```
    cd _artwork
    inkscape --file=git-cheatsheet-page01.svg --export-area-drawing --without-gui --export-pdf=git-cheatsheet-page01.pdf
    inkscape --file=git-cheatsheet-page02.svg --export-area-drawing --without-gui --export-pdf=git-cheatsheet-page02.pdf
    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=../print-pdf/git-cheatsheet.pdf git-cheatsheet-page01.pdf git-cheatsheet-page02.pdf
    rm git-cheatsheet-page01.pdf git-cheatsheet-page02.pdf
    ```

1. Commit the changes and submit a merge request

## Sharing the cheatsheet

Use the following URL to share and download the cheatsheet:

https://gitlab.com/gitlab-com/marketing/raw/master/design/print/git-cheatsheet/print-pdf/git-cheatsheet.pdf

### Short link (redirects to the URL above)

http://bit.ly/gitlab-cheatsheet

[ghostscript]: http://www.ghostscript.com/index.html
[inkscape]: https://inkscape.org/
