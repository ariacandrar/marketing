### Goal
Outline the goal of the post 

### Feature highlight posts should cover:

- Start with a high level intro of what we had before (this is important since we cannot assume someone is already a GitLab user)
- Clarify why it may not have been perfect or why we decided to improve it
- Introduce our new feature (explain what is and how it works)
- Share a use case of GitLab or a fictional one (describe how the feature gets used and what benefits it provides)
- Link to relevant docs
- Promote webcast when appropriate 

### Specific suggestions for this post:
Add additional information to help shape the direction the post goes in 
